<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Auth::routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        if (Auth::user()->role == 'Admin')
            return redirect('admin');
        elseif (Auth::user()->role == 'User')
            return redirect('user');
        else
            return redirect('error');
    });
    Route::get('error', function () {
        return "Sorry, you are unauthorized to access this page.";
    });
    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::view('/', 'role.admin');
        
        // Route::get('/product/create',function(){
        //     return 'ok';
        // });
//      Please put all what you want to do with Admin role in here
    });
    Route::group(['prefix' => 'user', 'middleware' => 'user'], function () {
        Route::view('/', 'role.user');

        // Route::get('/product/create',function(){
        //     return 'ok';
        // });
//      Please put all what you want to do with User role in here
    });
});

